/* ========== Importing Package Start ========== */

	// *=== Importing  jsonwebtoken package ===*
	const jwt = require("jsonwebtoken");

/* ========== Importing Package End   ========== */

const secret = "ECommerceAPI";


/* ========== Token Creation Start ========== */
module.exports.createAccessToken = (user) => {

	const data = {

		id: user._id,
		Email: user.Email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});

};
/* ========== Token Creation End   ========== */


/* ========== Token Verification Start ========== */
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		// Validate Token using the ".verify" method, decrypting the token using the secret code
		return jwt.verify(token, secret, (error, data) => {

			if (error) { // if JWT Token is not valid

				return res.send({auth: "failed"})

			} else { // if JWT Token is Valid

				// Allows the application to proceed with the next middleware function/callback function in the route
				// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
				next();
			}
		});

	} else { // If the JWT Token does not exist
		return res.send({auth: "failed"});
	}

};
/* ========== Token Verification End   ========== */



/* ========== Token Decryption Start ========== */
module.exports.decode = (token) => {

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) { // if JWT Token is not valid

				return null

			} else { // if JWT Token is Valid

				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			}

		});
	} else { // Token does not exist
		return null
	}
};
/* ========== Token Decryption End   ========== */