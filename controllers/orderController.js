/* ========== Importing Data from Models and auth.js Start ========== */
const Order = require ("../models/Order");
const User = require ("../models/User");
const Product = require ("../models/Product");
const auth = require ("../auth.js");
/* ========== Importing Data from Models and auth.js End   ========== */


/* ========== Creating Controller Function Start ========== */


// *=== Create New Order Start ===*
module.exports.createNewOrder = (reqBody, data) => {

 	return User.findById(data.userId).then(result => {

		if (data.isAdmin === true) { // if Admin = True return false

			return false

		} else { // if Admin = false


			// instantiate new Order from Order Object
			let newOrder = new Order ({

				userId: reqBody.userId,
				products: reqBody.products,
				totalAmount: reqBody.totalAmount 
			});

			// Then save the newOrder using the .save() method to saves the object to the database
			// after saving use the .then() method to proceed and catch the error if the Creation of Order is failed and return a String "Added New Porduct" if Successful
			return newOrder.save().then((order, error) => {


				if (error) { // if the Order creation is Failed

					return false

				} else { // if the Order creation is Successful

					return true

				}
			});
		}
	});
};
// *=== Create New Order End   ===*

// *=== Retrieve Authenticated User's Order Start ===*
module.exports.getUserOrder = (reqParams) => {

	return Order.find({userId: reqParams.userId}).then(result => {

		return result

	});

};
// *=== Retrive Authenticated User's Order End   ===*


// *=== Retrieve All Orders (Admin Only) Start ===*
module.exports.getAllOrders = (orderData) => {

	return Order.find({}).then(result => {

			return result
	});

};
// *=== Retrive All Orders (Admin Only) End   ===*

/*

// 


// controller for create order (Non-Admin)
module.exports.createOrder = async(data) => {

	let productPrice = await Product.findById(data.productId).then(product => product.Price);

	console.log(productPrice);

	//Save User Order
		const userSaveOrder = await User.findById(data.userId).then( user => {
			
			
			let totalAmountOrder = (data.quantity) * (productPrice)
			user.order.push(
					{	products: {
							productId: data.productId,
							quantity: data.quantity
							},
						totalAmount: totalAmountOrder

					}
			)


			return user.save().then( (user, error) => {
					if (error){
						return false
					} else {
						return true
					}
				})

			})

	//Save Product Orders
		let productSaveOrder = await Product.findById(data.productId).then(product => {

			let totalAmountOrder = (data.quantity) * (productPrice)

			product.orders.push(
				{
					customerId: data.userId,
					totalAmount: totalAmountOrder
				})

			return product.save().then( (order, error) => {
					if (error){
						return false
					} else {
						return true
					}
			})
		})
		
		if(userSaveOrder && productSaveOrder){
			return ('Order saved!')
		} else {
			return false
		}

}*/


/* ========== Creating Controller Function End   ========== */