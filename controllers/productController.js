 /* ========== Import Data Model file, auth.js & Other Package Start ========== */
const Product = require("../models/Product"); // Product data Model
const User = require("../models/User"); // User data Model
const auth = require("../auth"); // auth.js ► user jwt authentication

/* ========== Import Data Model file, auth.js & Other Package Start ========== */


/*=======================================================================*/


/* ========== Creating a Controller Function Start ========== */

// *=== Add New Product Controller Function Start ===*
module.exports.createNewProduct = (reqBody, userData) => {

											// is the document we get from the DB after successfully find the data using findById () mongoose method
 	return User.findById(userData.userId).then(result => {

		if (userData.isAdmin === false) { // if Admin = false return false

			return false

		} else { // if Admin = true

			// instantiate new Product from Product Object
			let newProduct = new Product ({

				Name: reqBody.Name,
				Description: reqBody.Description,
				Price: reqBody.Price

			});



			// Then save the newProduct using the .save() method to saves the object to the database
			// after saving use the .then() method to proceed and catch the error if the Creation of Product is failed and return a String "Added New Porduct" if Successful
			return newProduct.save().then((product, error) => {


				if (error) { // if the Product creation is Failed

					return false

				} else { // if the Product creation is Successful

					return true

				}
			});
		}
	});
};
// *=== Add New Product Controller Function End   ===*



// *=== Show All Product Controller Function Start ===*
module.exports.showAllProduct = () => {

	return Product.find({}).sort({isActive: "desc"}).then(result => {

		return result;

	});

};
// *=== Show All Product Controller Function End   ===*

// *=== Show All Active Product Controller Function Start ===*
module.exports.showActiveProduct = () => {

	return Product.find({isActive: true}).then(result => {

		return result;

	});
};
// *=== Show All Active Product Controller Function End   ===*

// *=== Show All Not Active Product Controller Function Start ===*
module.exports.showNotActiveProduct = (userData) => {

	return User.findById(userData.id).then(result => {

		if(result.isAdmin === true) {

			return Product.find({isActive: false}).then(result => {

				return result;

			});
		} else {
			return false;
		}

	});
};
// *=== Show All Not Active Product Controller Function End   ===*


// *=== Retrieve Specific/Single Product Controller Function Start ===*
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});

};
// *=== Retrieve Specific/Single Product Controller Function End   ===*


// *=== Updating Specific/Single Product Controller Function Start ===*
module.exports.updateSpecificProduct = (reqParams, reqBody, userData) => {

	// - find first the userId to ensure if the user is Admin from User Data Model
	// - use findById() mongoose method if true
	// - proceed to .then() method to process the result param
	return User.findById(userData.id).then(result => {

		if(result.isAdmin === true) { // if true proceed to

			// reassigning it to a variable to instantiate the object Product
			let updatedSpecificProduct = {

				Name: reqBody.Name,
				Description: reqBody.Description,
				Price: reqBody.Price

			};

			// - if the userId is found then find the Specific Product from Data Model
			// - use findByIdAndUpdate() method in order to save directly it to the Database
			// - found proceed to the .then() method to verify/catch if there is any error
			return Product.findByIdAndUpdate(reqParams.productId, updatedSpecificProduct).then((product, error) => {

				if(error) { // if error return false

					return false;

				} else { // if true return true

					return true;

				}
			});

		} else { // if result.isAdmin === false return false

			return false
		}

	});
};
// *=== Updating Specific/Single Product Controller Function End   ===*



// *=== Archiving Specific/Single Product Controller Function Start   ===*
module.exports.archiveProduct = (data, reqBody ) => {

	return Product.findById(data.productId).then(result => {

		if(data.payload === true) {

			let updateActiveField = {
				isActive : reqBody.isActive
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, error) => {

				if (error) {
					return false;
				} else {
					return true;
				}
			});

 	 	} else {
 	 		return false;
 	 	}
	});
}; 

// *=== Archiving Specific/Single Product Controller Function End   ===*

// *=== UnArchiving Specific/Single Product Controller Function Start   ===*
module.exports.unArchiveProduct = (productData, reqParams, userData) => {

	return Product.findById(productData.productId).then(result => {

		if(productData.payload === true) {

			let updateActiveField = {
				isActive : true
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, error) => {

				if (error) {
					return false;
				} else {
					return true;
				}
			});

 	 	} else {
 	 		return false;
 	 	}
	});
};
// *=== UnArchiving Specific/Single Product Controller Function End   ===*

/* ========== Creating a Controller Function End ========== */


/* Archive Product Orig Code
module.exports.archiveProduct = (productData, reqBody) => {

	return Product.findById(productData.productId).then(result => {

		if(productData.payload === true) {


			let updateIsActiveField = {

				isActive: reqBody.isActive
 	 		
 	 		}
			

			return Product.findByIdAndUpdate(result._id, updateIsActiveField).then((product, error) => {

				if(error) {

					return false

				} else {
					return true
				}

			});
		} else {

			return false

		}

	});

};

*/