/* ========== Import Data Model file, auth.js & Other Package Start ========== */
const User = require("../models/User"); // User data Model
const Product = require("../models/Product"); // User data Model
const auth = require("../auth"); // auth.js ► user jwt authentication
	
	// *=== Import bcrypt package ===*
	const bcrypt = require("bcrypt"); // for hashing password
/* ========== Import Data Model file, auth.js & Other Package End   ========== */


/*=======================================================================*/


/* ========== Creating a Controller Function Start ========== */


// *=== Check if Email is Exist Controller Start ===*
module.exports.checkEmailExists = (reqBody) => {

	return User.find({ Email : reqBody.Email }).then(result => {

		if(result.length > 0) {

			return true

		} else {

			return false
		}
	})
}
// *=== Check if Email is Exist Controller End   ===*


// *=== User Registration Controller Start ===*
module.exports.registerUser = (reqBody) => {

	// Instantiate a newUser Object
	let newUser = new User({

		Email: reqBody.Email,
		Password: bcrypt.hashSync(reqBody.Password, 10)
		/* ☼ bcrypt ► is a package and assigning it to a variable bcrypt in order to use the package
				   
				   ☼ .hashSync() method ► is a bcrypt method that is responsible for hashing a data
				   
				   ☼ Syntax:

				   		bcrypt.hashSync(<dataTObeHash>, <saltRound>)

				   			- saltround is the number of times to generate a password encryption 
		*/ 
	});

	// then return the newUser and use .save() method then use the .then() for catching up the error
	return newUser.save().then((user, error) => {

		if (error) { // if there is an error upon saving return FALSE
			return false
		} else { // if successful upon saving return true
			return true
		}

	});
}
// *=== User Registration Controller END   ===*


// *=== User Login / User Authentication Controller Start ===*
module.exports.loginUser = (reqBody) => {

					//findOne ► is a mongoose Method
		// modelsName.findOne ({criteria: value}) 
	return User.findOne({Email: reqBody.Email}).then(result => {

		if(result === null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.Password, result.Password);

			if (isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}
	});
}
// *=== User Login / User Authentication Controller End   ===*


// *=== Retrieve User Details Controller Start ===*
module.exports.getUserDetails = (reqBody) =>{

	return User.findById(reqBody.id).then(result => {

		result.Password  = "";

		return result

	})

};
// *=== Retrieve User Details Controller End   ===*


// *=== Set User's as Admin (Admin Only) Controller Start ===*
module.exports.setUserAsAdmin = (userData, reqBody) => {

	return User.findById(userData.userId).then(result => {

		if(userData.payload === true) {

			let updateIsAdminField = {

				isAdmin: reqBody.isAdmin

			}

			return User.findByIdAndUpdate(result._id, updateIsAdminField).then((user, error) => {

				if(error) {

					return false

				} else {
					return true
				}

			});
		} else {

			return false

		}

	});
};
// *=== Set User's as Admin (Admin Only) Controller End   ===*


// *=== SHow All Users (Admin Only) Start ===*
module.exports.showAllUsers = () => {

	return User.find({}).then(result => {

			return result
	});

};
// *=== SHow All Users (Admin Only) End   ===*

/* ========== Creating a Controller Function End   ========== */