/* ========== Importing Packages Start ========== */
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
/* ========== Importing Packages End ============ */


/* ========== Importing Routes file Path Start ============ */
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
/* ========== Importing Routes file Path End ============== */


// declaring  a Port
const port = 4000;

// Load the app
const app = express();



/********** MongoDB Connection Start **********/
mongoose.connect("mongodb+srv://admin:admin123@batch204-aspemarkjoseph.iwwi5jg.mongodb.net/s42-s46-ECommerceBackendAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

	// *=== Checking DB Connection Start ===*
		// Create a varibale db for mongoose.connection
	let db = mongoose.connection;

	db.on("error", () => console.error.bind(console, "Error"));
	db.once("open", () => console.log("Now Connected to MongoDb Atlas!"));
	// *=== Checking DB Connection End ===*

/********** MongoDB Connection End ************/

app.use(cors());
app.use(express.json());


/********** Loading Routes in the Server Start **********/
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
/********** Loading Routes in the Server End ************/



// Listen the App in port 4000
app.listen(port, () => {

	console.log(`API is now online on port: ${port}`);

});