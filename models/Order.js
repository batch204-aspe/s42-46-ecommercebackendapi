const mongoose = require ("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		// default: [true, "User Id is required"]
	},
	products: [{

		productId: {
			type: String
			
		},

		productName: {
			type: String,
			// default: [true, "Product Id is required"]
		},

		productPrice: {
			type: Number
			
		},

		quantity: {
			type: Number,
			// default: [true, "Quantity is required"]
		},

		subtotal: {
			type: Number
			
		}
	}],

	totalAmount: {
		type: Number
		 
	},

	purchasedOn: {
			type: Date,
			default: new Date()
	}

});

module.exports = mongoose.model("Order", orderSchema)