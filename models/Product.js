/* ========== Import Mongoose Package ========== */
const mongoose = require("mongoose");


/* ========== Declaring Product Schema ========== */
const productSchema = new mongoose.Schema ({

	Name: {
		type: String,
		default: [true, "Product Name is required"]
	},

	Description: {
		type: String,
		default: [true, "Product Description is required"]
	},

	Price: {
		type: Number,
		default: [true, "Product Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
	
	/*
	order: [{
		orderId: {
			type: String,
			required: [true, "order Id is required"]
		}
	}]
	*/
});


/* ========== Export Product Model ========== */
module.exports = mongoose.model("Product", productSchema)