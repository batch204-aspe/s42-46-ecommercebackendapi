/* ========== Import Mongoose Package ========== */
const mongoose = require("mongoose");

// Declaring User Schema
const userSchema = new mongoose.Schema({

	Email: {
		type: String,
		default: [true, "Email is required"]
	},

	Password: {
		type: String,
		default: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	}

	/*
	orders: [{
		products: [{

			productName: {
				type: String,
				 default: [true, "Product Name is required"]
			},

			quantity: {
				type: Number,
				default: [true, "Quantity is required"]
			}
		}],

		totalAmount: {
			type: Number,
			default: [true, "Total Amount is required"]
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		}

	}]
	*/
});

// Export User Model
module.exports = mongoose.model("User", userSchema)