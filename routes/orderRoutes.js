/* ========== Import Express Package ========== */
const express = require("express");
// *=== ReAssign the package ===*
const router = express.Router();

/* ========== Import userController.js & auth.js File Path Start ========== */
const orderController = require ("../controllers/orderController");
const auth = require ("../auth");
/* ========== Import userController.js & auth.js File Path Start ========== */


// *=== Creating New Order Routes Start ===*
router.post("/new-order", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	orderController.createNewOrder(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController));

});
// *=== Creating New Order Routes End   ===*


// *=== Retrieve User Order Routes Start ===*
router.get("/user-orders/:userId", (req, res) => {


	orderController.getUserOrder(req.params).then(resultFromController => res.send(resultFromController));

});
// *=== Retrieve User Order Routes End   ===*


// *=== Retrieve All Orders (Admin Only) Routes Start ===*
router.get('/show-all-orders', auth.verify, (req, res) => {


	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	} 

});
// *=== Retrieve All Orders (Admin Only) Routes End   ===*



// route for create an order (Non-Admin)
router.post('/checkout', auth.verify, (req,res) => {
	

	let data = {	
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
		
	}

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === false){
		orderController.createOrder(data).then(result => res.send(result))
	} else {
		res.send(true)
	} 

})

module.exports = router;