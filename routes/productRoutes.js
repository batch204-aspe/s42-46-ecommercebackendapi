/* ========== Import Express Package ========== */
const express = require("express");
// *=== ReAssign the package ===*
const router = express.Router();

/*========== Import userController.js & auth.js File Path Start ==========*/
const productController = require ("../controllers/productController");
const auth = require ("../auth");
/* ========== Import userController.js & auth.js File Path Start ========== */



/* ========== Creating Product Routes EndPoint Start ========== */


// *=== Adding New Product (ADMIN Only) Routes Start ===*
							//auth.verify is function form auth.js in order to ensure that the user is login as ADmin or RegUser
							// Note Auth is a file Name & verify is a function from Auth.js
							// use (.) dot in order to call/invoke a function 
router.post("/add-product", auth.verify, (req, res) => {

	// 1st assigning the auth.decode to a variable in order to use the decode function from auth.js file
	const userData = auth.decode(req.headers.authorization);

	productController.createNewProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController));

});
// *=== Adding New Product (ADMIN Only) Routes End   ===*


// *=== Show All Product Routes Start ===*
router.get("/all-product", (req, res) => {

	productController.showAllProduct().then(resultFromController => res.send(resultFromController));

});
// *=== Show All Product Routes End   ===*


// *=== Show All Active Product Routes Start ===*
router.get("/active-product", (req, res) => {

	productController.showActiveProduct().then(resultFromController =>res.send(resultFromController));
});
// *=== Show All Active Product Routes End   ===*


// *=== Show All Not Active Product Routes Start ===*
router.get("/not-active-product", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	productController.showNotActiveProduct(userData).then(resultFromController =>res.send(resultFromController));

	// productController.showNotActiveProduct().then(resultFromController =>res.send(resultFromController));
});
// *=== Show All Not Active Product Routes End   ===*


// *=== Retrieving Specific/Single Product Routes Start ===*
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});
// *=== Retrieving Specific/Single Product Routes End   ===*


// *=== Updating Specific/Single Product (ADMIN Only) Routes Start ===*
						//auth.verify is function form auth.js in order to ensure that the user is login as ADmin or RegUser
router.put("/:productId", auth.verify, (req, res) => {

	// 1st assigning the auth.decode to a variable in order to use the decode function from auth.js file
	const userData = auth.decode(req.headers.authorization)


	productController.updateSpecificProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));

});
// *=== Updating Specific/Single Product (ADMIN Only) Routes End   ===*


// *=== Archiving Specific/Single Product (ADMIN Only) Routes Start   ===*
router.put("/archive/:productId", auth.verify, (req, res) => {

	const data = {

		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin

	}

	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});
// *=== Archiving Specific/Single Product (ADMIN Only) Routes End   ===*


// *=== UnArchiving Specific/Single Product (ADMIN Only) Routes Start   ===*
router.put("/unarchive/:productId", auth.verify, (req, res) => {

	const userData = {

		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin

	}

	productController.unArchiveProduct(userData, req.params.productId).then(resultFromController => res.send(resultFromController));
});
// *=== UnArchiving Specific/Single Product (ADMIN Only) Routes End   ===*

/* ========== Creating Product Routes EndPoint End   ========== */



//  *=== Export Router ===*
module.exports = router;


/* Archive Product Orig Code
router.put("/archive/:productId", auth.verify, (req, res) => {

	const userData = {

		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin

	}

	productController.archiveProduct(userData, req.body).then(resultFromController => res.send(resultFromController));

});

*/