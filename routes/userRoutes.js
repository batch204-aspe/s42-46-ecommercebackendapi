/* ========== Import Express Package ========== */
const express = require("express");
// *=== ReAssign the package ===*
const router = express.Router();

/* ========== Import userController.js & auth.js File Path Start ========== */
const userController = require ("../controllers/userController");
const auth = require("../auth");
/* ========== Import userController.js & auth.js File Path Start ========== */

/*=====================================================================*/



/* ========== Creating User Routes EndPoint Start ========== */
	
	// *=== Check if Email Exist Route Start ===*
	router.post("/checkEmail", (req, res) => {

		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
	});
	// *=== Check if Email Exist Route End   ===*


	// *=== User Registration Route Start ===*
	router.post("/register", (req, res) => {

		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

	});
	// *=== User Registration Route END   ===*


	// *=== User Login / User Authentication Route Start ===*
	router.post("/login", (req, res) => {

		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

	});
	// *=== User Login / User Authentication Route End   ===*


	// *=== User Details Route Start ===*
	router.get("/user-details", auth.verify, (req, res) => {


		const userData = auth.decode(req.headers.authorization);

		userController.getUserDetails({id: userData.id}).then(resultFromController => res.send(resultFromController));

	});
	// *=== User Details Route End   ===*


	// *=== Set User's as Admin (Admin Only) Route Start ===*
	router.put("/set-admin/:userId", auth.verify, (req, res) => {

		const userData = {

			userId: req.params.userId,
			payload: auth.decode(req.headers.authorization).isAdmin
		}

		userController.setUserAsAdmin(userData, req.body).then(resultFromController => res.send(resultFromController));
	});
	// *=== Set User's as Admin (Admin Only) Route Start ===*


	// *=== Retrieve All Users (Admin Only) Routes Start ===*
	router.get('/show-all-users', auth.verify, (req, res) => {


		let userData = auth.decode(req.headers.authorization);

		if(userData.isAdmin === true){

			userController.showAllUsers().then(resultFromController => res.send(resultFromController))

		} else {

			res.send(false)

		} 
	});
	// *=== Retrieve All Users (Admin Only) Routes End   ===*

/* ========== Creating User Routes EndPoint End ============ */

//  *=== Export Router ===*
module.exports = router;